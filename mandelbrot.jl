using Images, Colors, ColorSchemes

# get the number of theps
function get_steps(c::Complex, max_steps)
  z = Complex(0.0, 0.0) # 0 + 0im
  for i=1:max_steps
    z = z*z+c
    if abs2(z) >= 4
      return i
    end
  end
  return max_steps+1
end

function get_color(colorscheme, step, max_steps)
  if step == max_steps+1
    return [0.0, 0.0, 0.0]
  end
  color = get(colorscheme, step, (1, max_steps))
  return [color.r, color.g, color.b]
end

function get_cmap(colorscheme, max_steps)
  colors = zeros(Float64, (3, max_steps+1))
  for i=1:max_steps
    colors[:,i] = get_color(colorscheme, i, max_steps)
  end
  colors[:,max_steps+1] = [0.0, 0.0, 0.0]
  return colors
end

function mandelbrot_plot()
  width = 1000
  height = 600

  max_steps = 50
  steps = zeros(Int, (height, width))

  # range of real values
  cr_min = -2.5
  cr_max = 1.5

  # range of imaginary values
  ci_min = -1.2

  range = cr_max - cr_min
  dot_size = range/width
  ci_max = ci_min + height*dot_size

  #println("cr: $cr_min - $cr_max")
  #println("ci: $ci_min - $ci_max")

  image = zeros(Float64, (3, height, width)) # black image
  complexes = zeros(Complex, (height, width))
  steps = zeros(Int, (height, width))
  colorscheme = ColorSchemes.inferno
  colorscheme_sized = get_cmap(colorscheme, max_steps)
  println("colorscheme_sized: $colorscheme_sized")
  x, y = 1,1  
  for ci=ci_min:dot_size:ci_max-dot_size
    x = 1
    for cr=cr_min:dot_size:cr_max-dot_size
      c = Complex(cr, ci)
      complexes[y, x] = c           
      x += 1
    end
    y += 1
  end
  steps = get_steps.(complexes, max_steps) 
  image = colorscheme_sized[:, steps]
 
  save("images/test.png", colorview(RGB, image))

end

mandelbrot_plot();