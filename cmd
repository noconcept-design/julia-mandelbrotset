julia
import Pkg; Pkg.add("Images")
import Pkg; Pkg.add("Colors")
import Pkg; Pkg.add("ColorSchemes")
import Pkg; Pkg.add("ImageIO")
import Pkg; Pkg.add("ImageMagick")
import Pkg; Pkg.add("BenchmarkTools")
include("mandelbrot.jl")

using BenchmarkTools
@benchmark mandelbrot_plot()